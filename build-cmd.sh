#!/bin/sh
stage="$(pwd)"
cd "$(dirname "$0")"
top="$(pwd)"

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

OPENSSL_VERSION="1.0.0g"
OPENSSL_SOURCE_DIR="openssl-$OPENSSL_VERSION"

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

build_linux()
{
   # "shared" means build shared and static, instead of just static.
   ./Configure shared no-idea linux-generic$1 -fno-stack-protector -m$1 --prefix="$stage" --libdir="lib/release"

   make
   make install

#appears to be  a set up for a debian package. Not needed for Kokua
#   pushd "$stage/lib/release/pkgconfig"
#   find . -type f  -name '*.pc' \
#          -exec perl -pi -e "\$baz=quotemeta('$stage');s/\$baz/\\$\{PREBUILD_DIR\}/g" {} \;
#   popd

  # By default, 'make install' leaves even the user write bit off.
  # This causes trouble for us down the road, along about the time
  # the consuming build tries to strip libraries.
  chmod u+w "$stage/lib/release"/libcrypto.so.* "$stage/lib/release"/libssl.so.*

}

cd "$OPENSSL_SOURCE_DIR"
    case "$AUTOBUILD_PLATFORM" in
        "windows")
            load_vsvars

            # disable idea cypher per Phoenix's patent concerns (DEV-22827)
            perl Configure VC-WIN32 no-asm no-idea

            # Not using NASM
            ./ms/do_ms.bat

            nmake -f ms/ntdll.mak

            mkdir -p "$stage/lib/debug"
            mkdir -p "$stage/lib/release"

            cp "out32dll/libeay32.lib" "$stage/lib/debug"
            cp "out32dll/ssleay32.lib" "$stage/lib/debug"
            cp "out32dll/libeay32.lib" "$stage/lib/release"
            cp "out32dll/ssleay32.lib" "$stage/lib/release"

            cp out32dll/{libeay32,ssleay32}.dll "$stage/lib/debug"
            cp out32dll/{libeay32,ssleay32}.dll "$stage/lib/release"

            mkdir -p "$stage/include/openssl"

            # These files are symlinks in the SSL dist but just show up as text files
            # on windows that contain a string to their source.  So run some perl to
            # copy the right files over.
            perl ../copy-windows-links.pl "include/openssl" "$stage/include/openssl"
        ;;
        "darwin")
            opts='-arch i386 -iwithsysroot /Developer/SDKs/MacOSX10.5.sdk -mmacosx-version-min=10.5'
            export CFLAGS="$opts"
            export CXXFLAGS="$opts"
            export LDFLAGS="$opts"
            ./Configure no-idea no-shared no-gost 'debug-darwin-i386-cc' --prefix="$stage"
            make depend
            make
            make install
        ;;
        "linux")
           build_linux 32
        ;;
        "linux64")
           build_linux 64
        ;;

    esac
    mkdir -p "$stage/LICENSES"
    cp LICENSE "$stage/LICENSES/openssl.txt"
cd "$top"
README_DIR="$stage/autobuild-bits"
README_FILE="$README_DIR/README-Version-3p-openssl"
mkdir -p $README_DIR
cat $top/.hg/hgrc|grep default |sed  -e "s/default = ssh:\/\/hg@/https:\/\//" > $README_FILE
echo "Commit $(hg id -i)" >> $README_FILE

pass

